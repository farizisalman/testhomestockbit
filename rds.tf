resource "aws_db_instance" "mysql-db-restapi" {
  identifier           = "restdb"
  storage_type         = "gp2"
  allocated_storage    = 20
  engine               = "mysql"
  engine_version       = "8.0"
  instance_class       = "db.t3.micro"
  port                 = "3306"
  name                 = "restapidb"
  username             = var.username
  password             = var.password
  parameter_group_name = "default.mysql8.0"
  skip_final_snapshot  = true
  publicly_accessible  = true

  tags = {
    name = "MySQL DB for Rest API"
  }
}
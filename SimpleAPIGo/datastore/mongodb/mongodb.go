package mongodb

import (
	"SimpleAPIGo/config"
	"SimpleAPIGo/datastore"
	"context"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

type MongoDB struct {
	config  *config.DatabaseConfig
	mongodb *mongo.Database
}

type user struct {
	Username string `bson:"username"`
	Password string `bson:"password"`
	FullName string `bson:"full_name"`
	Picture  string `bson:"picture"`
}

func New(cfg config.DatabaseConfig) (*MongoDB, error) {
	username_password := ""

	if cfg.User != "" {
		username_password = fmt.Sprintf("%s:%s@", cfg.User, cfg.Password)
	}

	time.Sleep(time.Second * 10)

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Minute)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(fmt.Sprintf("mongodb://%s%s:%d/", username_password, cfg.Host, cfg.Port)))

	if err != nil {
		return nil, err
	}

	pingctx, _ := context.WithTimeout(context.Background(), 30*time.Second)
	err = client.Ping(pingctx, readpref.Primary())

	if err != nil {
		return nil, err
	}

	return &MongoDB{
		config:  &cfg,
		mongodb: client.Database(cfg.Schema),
	}, nil
}

func (m *MongoDB) GetUser(User datastore.User) ([]datastore.User, error) {
	matchQuery := bson.M{}

	if User.Username != "" {
		matchQuery[datastore.USERNAME_FIELD] = User.Username
	}

	if User.Password != "" {
		matchQuery[datastore.PASSWORD_FIELD] = User.Password
	}

	if User.FullName != "" {
		matchQuery[datastore.FULL_NAME_FIELD] = User.FullName
	}

	if User.Picture != "" {
		matchQuery[datastore.PICTURE_FIELD] = User.Picture
	}

	curr, err := m.mongodb.Collection(datastore.USER_TABLE).Find(context.Background(), matchQuery)

	if err != nil {
		return nil, err
	}

	defer curr.Close(context.Background())

	var users []datastore.User
	for curr.Next(context.Background()) {
		var user user

		err = curr.Decode(&user)

		if err != nil {
			return nil, err
		}

		users = append(users, datastore.User{
			Username: user.Username,
			Password: user.Password,
			FullName: user.FullName,
			Picture:  user.Picture,
		})
	}

	return users, err
}

func (m *MongoDB) UpdateUser(Username string, NewData datastore.User) error {
	matchQuery := bson.M{
		datastore.USERNAME_FIELD: Username,
	}

	dataToUpdate := bson.D{}

	if NewData.Password != "" {
		dataToUpdate = append(dataToUpdate, bson.E{
			Key:   datastore.PASSWORD_FIELD,
			Value: NewData.Password,
		})
	}

	if NewData.FullName != "" {
		dataToUpdate = append(dataToUpdate, bson.E{
			Key:   datastore.FULL_NAME_FIELD,
			Value: NewData.FullName,
		})
	}

	if NewData.Picture != "" {
		dataToUpdate = append(dataToUpdate, bson.E{
			Key:   datastore.PICTURE_FIELD,
			Value: NewData.Picture,
		})
	}

	_, err := m.mongodb.Collection(datastore.USER_TABLE).UpdateOne(context.Background(), matchQuery, bson.D{
		{
			Key:   "$set",
			Value: dataToUpdate,
		},
	})

	return err
}

func (m *MongoDB) InsertUser(User datastore.User) error {
	_, err := m.mongodb.Collection(datastore.USER_TABLE).InsertOne(context.Background(), user{
		Username: User.Username,
		Password: User.Password,
		FullName: User.FullName,
		Picture:  User.Picture,
	})

	return err
}

func (m *MongoDB) DeleteUser(Username string) error {
	_, err := m.mongodb.Collection(datastore.USER_TABLE).DeleteOne(context.Background(), bson.M{
		datastore.USERNAME_FIELD: Username,
	})

	return err
}

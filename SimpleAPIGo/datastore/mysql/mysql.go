package mysql

import (
	"SimpleAPIGo/config"
	"SimpleAPIGo/datastore"
	"fmt"
	"log"
	"os"
	"time"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

type user struct {
	Username string `gorm:"column:username"`
	Password string `gorm:"column:password"`
	FullName string `gorm:"column:full_name"`
	Picture  string `gorm:"column:picture"`
}

type MysqlDB struct {
	config  *config.DatabaseConfig
	mysqldb *gorm.DB
}

func New(conf config.DatabaseConfig) (*MysqlDB, error) {
	dbConfig := fmt.Sprintf(
		"%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True&loc=Local",
		conf.User,
		conf.Password,
		conf.Host,
		conf.Port,
		conf.Schema,
	)

	logMode := logger.Info

	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold:             time.Second, // Slow SQL threshold
			LogLevel:                  logMode,     // Log level
			IgnoreRecordNotFoundError: true,        // Ignore ErrRecordNotFound error for logger
			Colorful:                  true,
		},
	)

	cfg := &gorm.Config{
		SkipDefaultTransaction: true,
		Logger:                 newLogger,
	}

	inst, err := gorm.Open(
		mysql.Open(dbConfig),
		cfg,
	)

	return &MysqlDB{
		config:  &conf,
		mysqldb: inst.Debug(),
	}, err
}

func (m *MysqlDB) GetUser(User datastore.User) ([]datastore.User, error) {
	inst := m.mysqldb.Table(datastore.USER_TABLE).Select(fmt.Sprintf("%s, %s, %s, %s", datastore.USERNAME_FIELD, datastore.PASSWORD_FIELD, datastore.FULL_NAME_FIELD, datastore.PICTURE_FIELD))

	if User.Username != "" {
		inst = inst.Where(datastore.USERNAME_FIELD+" = ?", User.Username)
	}

	if User.FullName != "" {
		inst = inst.Where(datastore.FULL_NAME_FIELD+" = ?", User.FullName)
	}

	if User.Password != "" {
		inst = inst.Where(datastore.PASSWORD_FIELD+" = ?", User.Password)
	}

	if User.Picture != "" {
		inst = inst.Where(datastore.PICTURE_FIELD+" = ?", User.Picture)
	}

	rows, err := inst.Rows()

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	var results []datastore.User
	for rows.Next() {
		var user user

		err = rows.Scan(&user.Username, &user.Password, &user.FullName, &user.Picture)

		if err != nil {
			return nil, err
		}

		results = append(results, datastore.User{
			Username: user.Username,
			Password: user.Password,
			FullName: user.FullName,
			Picture:  user.Picture,
		})
	}

	return results, nil
}

func (m *MysqlDB) UpdateUser(Username string, NewData datastore.User) error {
	dataToUpdate := map[string]interface{}{}

	if NewData.Password != "" {
		dataToUpdate[datastore.PASSWORD_FIELD] = NewData.Password
	}

	if NewData.FullName != "" {
		dataToUpdate[datastore.FULL_NAME_FIELD] = NewData.FullName
	}

	if NewData.Picture != "" {
		dataToUpdate[datastore.PICTURE_FIELD] = NewData.Picture
	}

	return m.mysqldb.Table(datastore.USER_TABLE).Updates(dataToUpdate).Where(datastore.USERNAME_FIELD+" = ?", Username).Error
}

func (m *MysqlDB) InsertUser(User datastore.User) error {
	createData := user{
		Username: User.Username,
		Password: User.Password,
		FullName: User.FullName,
		Picture:  User.Picture,
	}

	return m.mysqldb.Table(datastore.USER_TABLE).Create(createData).Error
}

func (m *MysqlDB) DeleteUser(Username string) error {
	return m.mysqldb.Exec(fmt.Sprintf("DELETE FROM %s WHERE %s = ?", datastore.USER_TABLE, datastore.USERNAME_FIELD), Username).Error
}

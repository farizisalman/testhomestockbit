package datastore

const (
	USERNAME_FIELD  = "username"
	PASSWORD_FIELD  = "password"
	FULL_NAME_FIELD = "full_name"
	PICTURE_FIELD   = "picture"
	USER_TABLE      = "users"
)

type DataStore interface {
	GetUser(User User) ([]User, error)
	UpdateUser(Username string, NewData User) error
	InsertUser(User User) error
	DeleteUser(Username string) error
}

type User struct {
	Username string `json:"username"`
	Password string `json:"-"`
	FullName string `json:"full_name"`
	Picture  string `json:"picture"`
}

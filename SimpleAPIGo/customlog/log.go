package customlog

import (
	"io"
	"log"
	"os"
)

var (
	Info *log.Logger
	Error *log.Logger
	infoHandle io.Writer = os.Stdout
	errorHandle io.Writer = os.Stderr
)

const (
	INFO_PREFIX = "INFO: "
	ERROR_PREFIX = "ERROR: "
)

func init() {
	Info = log.New(infoHandle,
		INFO_PREFIX,
		log.Ldate|log.Ltime|log.Llongfile)

	Error = log.New(errorHandle,
		ERROR_PREFIX,
		log.Ldate|log.Ltime|log.Llongfile)
}
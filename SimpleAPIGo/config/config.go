package config

import (
	"fmt"
	"os"

	"github.com/spf13/viper"
)

type AppConfig struct {
	Port int
}

type DatabaseConfig struct {
	Engine   string
	Host     string
	User     string
	Password string
	Schema   string
	Port     int
}

func init() {
	cdir, _ := os.Getwd()

	viper.AddConfigPath(fmt.Sprintf("%s/config", cdir))
	viper.SetConfigName("global.json")
	viper.SetConfigType("json")

	err := viper.ReadInConfig()

	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s", err.Error()))
	}
}

func LoadAppConfig() AppConfig {
	return AppConfig{
		Port: viper.GetInt("port"),
	}
}

func LoadDatabaseConfig() DatabaseConfig {
	setting := "database"

	return DatabaseConfig{
		Engine:   viper.GetString(setting + ".engine"),
		Host:     viper.GetString(setting + ".host"),
		User:     viper.GetString(setting + ".username"),
		Password: viper.GetString(setting + ".password"),
		Schema:   viper.GetString(setting + ".schema"),
		Port:     viper.GetInt(setting + ".port"),
	}
}

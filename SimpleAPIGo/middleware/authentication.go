package middleware

import (
	"SimpleAPIGo/customlog"
	"fmt"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

const (
	DATASTORE  = "mongo_data_store"
	REQUESTID  = "request_id"
	USERNAME   = "username"
	SECRET_KEY = "test_case_majoo"
)

const (
	MESSAGE_RESPONSE_FIELD = "message"
	UNAUTHORIZED_MESSAGE   = "unauthorized"
)

func AuthenticationJWT(c *gin.Context) {
	requestID := c.MustGet(REQUESTID).(string)

	auth := c.GetHeader("Authorization")

	if !strings.Contains(auth, "Bearer") {
		c.JSON(http.StatusUnauthorized, gin.H{
			MESSAGE_RESPONSE_FIELD: UNAUTHORIZED_MESSAGE,
		})
		c.Abort()
		return
	}

	tokenString := strings.ReplaceAll(auth, "Bearer ", "")

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if method, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Signing method invalid")
		} else if method != jwt.SigningMethodHS256 {
			return nil, fmt.Errorf("Signing method invalid")
		}

		return []byte(SECRET_KEY), nil
	})

	if err != nil {
		customlog.Error.Printf("%s : Error authenticate token in authentication middleware : %s", requestID, err)
		c.JSON(http.StatusUnauthorized, gin.H{
			MESSAGE_RESPONSE_FIELD: UNAUTHORIZED_MESSAGE,
		})
		c.Abort()
		return
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok || !token.Valid {
		c.JSON(http.StatusUnauthorized, gin.H{
			MESSAGE_RESPONSE_FIELD: UNAUTHORIZED_MESSAGE,
		})
		c.Abort()
		return
	}

	c.Set(USERNAME, claims["Username"])
	c.Next()
}

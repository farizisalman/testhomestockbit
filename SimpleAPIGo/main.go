package main

import (
	"SimpleAPIGo/config"
	"SimpleAPIGo/controller"
	"SimpleAPIGo/customlog"
	"SimpleAPIGo/datastore"
	"SimpleAPIGo/datastore/mongodb"
	"SimpleAPIGo/datastore/mysql"
	"SimpleAPIGo/middleware"
	"fmt"
	"os"
	"strconv"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func main() {
	if _, err := os.Stat("profilePicture"); os.IsNotExist(err) {
		os.Mkdir("profilePicture", os.ModeDir)
	}

	appConfig := config.LoadAppConfig()

	router := gin.New()
	router.Use(gin.Logger(), gin.Recovery())

	configCors := cors.DefaultConfig()
	configCors.AllowAllOrigins = true
	configCors.AddAllowHeaders("Authorization")

	dataStore, err := NewDatabase()

	if err != nil {
		customlog.Error.Fatalf("Error initiate database connection : %s", err.Error())
	}

	router.Use(cors.New(configCors))

	router.Use(func(c *gin.Context) {
		c.Set(middleware.DATASTORE, dataStore)
		c.Next()
	})

	router.Use(func(c *gin.Context) {
		requestID := strconv.FormatInt(time.Now().UTC().UnixNano(), 10)
		c.Set(middleware.REQUESTID, requestID)
	})

	router.POST("/login", controller.Login)
	router.GET("/user", middleware.AuthenticationJWT, controller.GetUser)
	router.POST("/user", middleware.AuthenticationJWT, controller.AddUser)
	router.PUT("/user", middleware.AuthenticationJWT, controller.UpdateUser)
	router.DELETE("/user", middleware.AuthenticationJWT, controller.DeleteUser)
	router.POST("/user/picture", middleware.AuthenticationJWT, controller.UploadPicture)
	router.GET("/user/picture", middleware.AuthenticationJWT, controller.GetPicture)

	err = router.Run(fmt.Sprintf(":%d", appConfig.Port))

	if err != nil {
		customlog.Error.Fatalf("Error run API : %s", err.Error())
	}
}

func NewDatabase() (datastore.DataStore, error) {
	config := config.LoadDatabaseConfig()

	if config.Engine == "mysql" {
		return mysql.New(config)
	}

	return mongodb.New(config)
}

package controller

import (
	"SimpleAPIGo/customlog"
	"SimpleAPIGo/datastore"
	"SimpleAPIGo/middleware"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

type LoginRequest struct {
	Username string `json:"username" form:"username" binding:"required"`
	Password string `json:"password" form:"password" binding:"required"`
}

type AddUserRequest struct {
	Username string `json:"username" form:"username" binding:"required"`
	Password string `json:"password" form:"password" binding:"required"`
	FullName string `json:"full_name" form:"full_name" binding:"required"`
}

type UpdateUserRequest struct {
	Password string `json:"password" form:"password"`
	FullName string `json:"full_name" form:"full_name"`
}

type GetUserRequest struct {
	Username string `json:"username" form:"username"`
	FullName string `json:"full_name" form:"full_name"`
}

const (
	SUCCESS_MESSAGE               = "success"
	INTERNAL_SERVER_ERROR_MESSAGE = "there is an internal server error, please try again later and save the request id if its still continue"
	MESSAGE_RESPONSE_FIELD        = "message"
	DATA_RESPONSE_FIELD           = "data"
	REQUEST_ID_FIELD              = "request_id"
)

type JWTClaims struct {
	jwt.StandardClaims
	Username string `json:"Username"`
}

func Login(c *gin.Context) {
	dataStore := c.MustGet(middleware.DATASTORE).(datastore.DataStore)
	requestID := c.MustGet(middleware.REQUESTID).(string)

	var request LoginRequest

	err := c.ShouldBind(&request)

	response := gin.H{
		MESSAGE_RESPONSE_FIELD: "unknown error",
		REQUEST_ID_FIELD:       requestID,
	}

	if err != nil {
		customlog.Error.Printf("%s : Error parse request in Login controller : %s", requestID, err)
		response[MESSAGE_RESPONSE_FIELD] = "username and password needed"

		c.JSON(http.StatusBadRequest, response)
		return
	}

	results, err := dataStore.GetUser(datastore.User{
		Username: request.Username,
		Password: request.Password,
	})

	if err != nil {
		customlog.Error.Printf("%s : Error Get User In Login controller : %s", requestID, err)
		response[MESSAGE_RESPONSE_FIELD] = INTERNAL_SERVER_ERROR_MESSAGE

		c.JSON(http.StatusInternalServerError, response)
		return
	}

	if len(results) <= 0 {
		response[MESSAGE_RESPONSE_FIELD] = "Wrong username or password"

		c.JSON(http.StatusBadRequest, response)
		return
	}

	claims := JWTClaims{
		StandardClaims: jwt.StandardClaims{
			Issuer:    "Test Case Majoo",
			ExpiresAt: time.Now().Add(time.Hour * 6).Unix(),
		},
		Username: results[0].Username,
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	signedToken, err := token.SignedString([]byte(middleware.SECRET_KEY))

	if err != nil {
		customlog.Error.Printf("%s : Error generate token in Login controller : %s", requestID, err)
		response[MESSAGE_RESPONSE_FIELD] = INTERNAL_SERVER_ERROR_MESSAGE

		c.JSON(http.StatusInternalServerError, response)
		return
	}

	response[MESSAGE_RESPONSE_FIELD] = SUCCESS_MESSAGE
	response[DATA_RESPONSE_FIELD] = gin.H{
		"user":  results[0],
		"token": signedToken,
	}

	c.JSON(http.StatusBadRequest, response)
	return
}

func GetUser(c *gin.Context) {
	dataStore := c.MustGet(middleware.DATASTORE).(datastore.DataStore)
	requestID := c.MustGet(middleware.REQUESTID).(string)

	var request GetUserRequest

	err := c.ShouldBind(&request)

	response := gin.H{
		MESSAGE_RESPONSE_FIELD: "unknown error",
		REQUEST_ID_FIELD:       requestID,
	}

	if err != nil {
		customlog.Error.Printf("%s : Error parse request in Login controller : %s", requestID, err)
		response[MESSAGE_RESPONSE_FIELD] = "username and password needed"

		c.JSON(http.StatusBadRequest, response)
		return
	}

	results, err := dataStore.GetUser(datastore.User{
		Username: request.Username,
		FullName: request.FullName,
	})

	if err != nil {
		customlog.Error.Printf("%s : Error Get User In Login controller : %s", requestID, err)
		response[MESSAGE_RESPONSE_FIELD] = INTERNAL_SERVER_ERROR_MESSAGE

		c.JSON(http.StatusInternalServerError, response)
		return
	}

	response[MESSAGE_RESPONSE_FIELD] = SUCCESS_MESSAGE
	response[DATA_RESPONSE_FIELD] = results

	c.JSON(http.StatusBadRequest, response)
	return
}

func AddUser(c *gin.Context) {
	dataStore := c.MustGet(middleware.DATASTORE).(datastore.DataStore)
	requestID := c.MustGet(middleware.REQUESTID).(string)

	var request AddUserRequest

	err := c.ShouldBind(&request)

	response := gin.H{
		MESSAGE_RESPONSE_FIELD: "unknown error",
		REQUEST_ID_FIELD:       requestID,
	}

	if err != nil {
		customlog.Error.Printf("%s : Error parse request in AddUser controller : %s", requestID, err)
		response[MESSAGE_RESPONSE_FIELD] = "please check your parameter"

		c.JSON(http.StatusBadRequest, response)
		return
	}

	users, err := dataStore.GetUser(datastore.User{
		Username: request.Username,
	})

	if err != nil {
		customlog.Error.Printf("%s : Error get user in AddUser controller : %s", requestID, err)
		response[MESSAGE_RESPONSE_FIELD] = INTERNAL_SERVER_ERROR_MESSAGE

		c.JSON(http.StatusInternalServerError, response)
		return
	}

	if len(users) > 0 {
		response[MESSAGE_RESPONSE_FIELD] = "username already exists"

		c.JSON(http.StatusBadRequest, response)
		return
	}

	err = dataStore.InsertUser(datastore.User{
		Username: request.Username,
		Password: request.Password,
		FullName: request.FullName,
	})

	if err != nil {
		customlog.Error.Printf("%s : Error insert user in AddUser controller : %s", requestID, err)
		response[MESSAGE_RESPONSE_FIELD] = INTERNAL_SERVER_ERROR_MESSAGE

		c.JSON(http.StatusInternalServerError, response)
		return
	}

	response[MESSAGE_RESPONSE_FIELD] = SUCCESS_MESSAGE

	c.JSON(http.StatusOK, response)
	return
}

func UpdateUser(c *gin.Context) {
	dataStore := c.MustGet(middleware.DATASTORE).(datastore.DataStore)
	requestID := c.MustGet(middleware.REQUESTID).(string)
	username := c.MustGet(middleware.USERNAME).(string)

	var request UpdateUserRequest

	err := c.ShouldBind(&request)

	response := gin.H{
		MESSAGE_RESPONSE_FIELD: "unknown error",
		REQUEST_ID_FIELD:       requestID,
	}

	if err != nil {
		customlog.Error.Printf("%s : Error parse request in UpdateUser controller : %s", requestID, err)
		response[MESSAGE_RESPONSE_FIELD] = "please check your parameter"

		c.JSON(http.StatusBadRequest, response)
		return
	}

	err = dataStore.UpdateUser(username, datastore.User{
		Password: request.Password,
		FullName: request.FullName,
	})

	if err != nil {
		customlog.Error.Printf("%s : Error update user in UpdateUser controller : %s", requestID, err)
		response[MESSAGE_RESPONSE_FIELD] = INTERNAL_SERVER_ERROR_MESSAGE

		c.JSON(http.StatusInternalServerError, response)
		return
	}

	response[MESSAGE_RESPONSE_FIELD] = SUCCESS_MESSAGE

	c.JSON(http.StatusOK, response)
}

func DeleteUser(c *gin.Context) {
	dataStore := c.MustGet(middleware.DATASTORE).(datastore.DataStore)
	requestID := c.MustGet(middleware.REQUESTID).(string)
	username := c.MustGet(middleware.USERNAME).(string)

	response := gin.H{
		MESSAGE_RESPONSE_FIELD: "unknown error",
		REQUEST_ID_FIELD:       requestID,
	}

	err := dataStore.DeleteUser(username)

	if err != nil {
		customlog.Error.Printf("%s : Error delete user in DeleteUser controller : %s", requestID, err)
		response[MESSAGE_RESPONSE_FIELD] = INTERNAL_SERVER_ERROR_MESSAGE

		c.JSON(http.StatusInternalServerError, response)
		return
	}

	response[MESSAGE_RESPONSE_FIELD] = SUCCESS_MESSAGE

	c.JSON(http.StatusOK, response)
}

func GetPicture(c *gin.Context) {
	dataStore := c.MustGet(middleware.DATASTORE).(datastore.DataStore)
	requestID := c.MustGet(middleware.REQUESTID).(string)
	username := c.MustGet(middleware.USERNAME).(string)

	users, err := dataStore.GetUser(datastore.User{
		Username: username,
	})

	if err != nil {
		customlog.Error.Printf("%s : Error get user in GetPicture controller : %s", requestID, err)
		c.String(http.StatusInternalServerError, INTERNAL_SERVER_ERROR_MESSAGE)
		return
	}

	if len(users) <= 0 {
		c.String(http.StatusBadRequest, "user not found")
		return
	}

	if users[0].Picture != "" {
		c.File("profilePicture/" + users[0].Picture)
		return
	}

	c.String(http.StatusOK, "")
}

func UploadPicture(c *gin.Context) {
	dataStore := c.MustGet(middleware.DATASTORE).(datastore.DataStore)
	requestID := c.MustGet(middleware.REQUESTID).(string)
	username := c.MustGet(middleware.USERNAME).(string)

	pictureFile, err := c.FormFile("picture")

	response := gin.H{
		MESSAGE_RESPONSE_FIELD: "unknown error",
		REQUEST_ID_FIELD:       requestID,
	}

	if err != nil {
		customlog.Error.Printf("%s : Error parse file in UploadPicture controller : %s", requestID, err)
		response[MESSAGE_RESPONSE_FIELD] = INTERNAL_SERVER_ERROR_MESSAGE

		c.JSON(http.StatusInternalServerError, response)
		return
	}

	err = c.SaveUploadedFile(pictureFile, "profilePicture/"+pictureFile.Filename)

	if err != nil {
		customlog.Error.Printf("%s : Error save file in UploadPicture controller : %s", requestID, err)
		response[MESSAGE_RESPONSE_FIELD] = INTERNAL_SERVER_ERROR_MESSAGE

		c.JSON(http.StatusInternalServerError, response)
		return
	}

	err = dataStore.UpdateUser(username, datastore.User{
		Picture: pictureFile.Filename,
	})

	if err != nil {
		customlog.Error.Printf("%s : Error update picture in UploadPicture controller : %s", requestID, err)
		response[MESSAGE_RESPONSE_FIELD] = INTERNAL_SERVER_ERROR_MESSAGE

		c.JSON(http.StatusInternalServerError, response)
		return
	}

	response[MESSAGE_RESPONSE_FIELD] = SUCCESS_MESSAGE

	c.JSON(http.StatusOK, response)
}

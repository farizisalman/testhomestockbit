#Create and bootstrap restvm
resource "aws_instance" "restvm" {
  ami                         = data.aws_ssm_parameter.restvm-ami.value
  instance_type               = "t3.micro"
  key_name                    = aws_key_pair.restvm-key.key_name
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.sg.id]
  subnet_id                   = aws_subnet.subnet.id
  provisioner "remote-exec" {
    inline = [
      "sudo yum -y update",
      "sudo yum -y install docker && sudo systemctl enable docker && sudo systemctl start docker",
      "sudo yum -y install git && git clone https://gitlab.com/farizisalman/testhomestockbit.git",
      "cd testhomestockbit/SimpleAPIGo && sudo docker build -t restapi:latest .",
      "sudo docker run -d -p 80:80 restapi"
    ]
    connection {
      type        = "ssh"
      user        = "ec2-user"
      private_key = file("~/.ssh/id_rsa")
      host        = self.public_ip
    }
  }
  tags = {
    Name = "restvm"
  }
}